---
title: 避难所
date: 2022-10-25 17:16:27
article:
    licenses: false
comment: false
donates: false
---
> 啊咧，怎么失联了？🤔

为了避免不可抗力的因素，我们建议您在遇到失联情况前，尽快前往我们的避难所。

[illusory skin Telegram 通知频道](https://t.me/iskin_toc) （2024/12/25更新）用于发布 illusory skin 相关更新通知，我们只会在这里发布论坛邀请码。

[illusory skin Telegram 讨论群](https://t.me/+b8bpNeR45LRjODc9) 男皮水群。

---

你还可以通过以下方式联系我们：

邮件到 [i@101069.xyz](mailto:i@101069.xyz) 