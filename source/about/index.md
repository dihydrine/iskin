---
title: 关于本站
date: 2021-03-26 01:45:17
article:
    licenses: false
comment: false
donates: false
---
> 欢迎访问 illusory skin，很高兴遇见你！🤝

✍️  **illusory skin** 是一个公益储存男性皮物及催眠类文章与图片的站点，取自 **幻皮吧** 。

本站域名：

- 自设域名：[101069.xyz](https://101069.xyz)，iskin.xyz（已弃用）
- Vercel 官方域名：[np.vercel.app](https://np.vercel.app/)（该二级域名被墙）

<!-- more -->

## 🏠 关于本站

这里收录了互联网的皮物类文章，图片...
本站启用了 Umami 自建站点统计，除此以外不会有任何追踪行为。
为同好干杯~

### 本站结构

- 源站和随机图片背景部署在 Vercel 上，即 Amazon Global Accelerator（AGA）加速。
- 使用 Hexo 构建静态网站，托管在 Bitbucket 上，自动部署到 Vercel 全球网络。
- 直链图片和视频都储存在 BackBlaze 对象存储桶中，并以 Cloudflare 提供服务。

## 👨‍💻 站长是谁

Dihydrine，二水化合物，你可以叫我二水，是以前的热水。

## 📬 联系我

邮件到 [i@10109.xyz](mailto:i@10109.xyz) 

## 🎫 邀请码获取

请查看 [邀请码获取页面](/donate)

## 技巧👇

💡 你可以点击文章目录快速定位。

🏷️  你可以通过标签找到你想要看的小说。

📋  你可以使用图廊看看新收录的图片。

💬  增加了可匿名的评论系统 Waline ，欢迎留下你的足迹。

🌱 **illusory skin** 刚刚上路，会不断改进的。 🏃

希望这个站点可以满足你一些“需求”

😘 Enjoy~

