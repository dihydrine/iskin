---
title: Discover
tags:
- 皮物
- 芯氼
categories:
- - 坑
cover: https://static.101069.xyz/image/discover.jpg
date: '2020-09-10 22:21:51'
updated: '2020-09-10 22:21:51'
reprint: true
---

>这是一篇转载文，作者是 [芯氼](/tags/芯氼/)

<!-- more -->

下午五点一刻，夕阳西垂，余晖在绿茵场附近的河面上撒下遍地金光，整个操场上锻炼的学生们开始尽数散去往食堂走，可能是今天食堂出了什么新活动，所以很多人决定吃了晚饭再回家过周末，包括陆子麒。

陆子麒抓了抓头发，一脸丧着往操场走，刚刚理科老师抱团过来布置了一大堆作业，他苦着脸，可那个他要去找的人却在篮球场上一无所知，他逆着人流走到了篮球场里，发现今天教练提早把篮球生聚了起来，教练在篮球场中间吹着哨，举手示意。

“子麒，帮我放下球。”一个满头大汗的人抖了抖篮球服，擦了把汗，笑着把篮球扔到了他手里，这就是他今天要找的人，肖放，现役校篮球队队长。
陆子麒气愤地朝肖放比了个中指，转着球往器材室走，器材室半开着，他走进去把球往筐里一扔，正想出门，也不知道哪里来的一阵妖风，一吹猛地把器材室的门给带上了，把陆子麒困在了里面。
“艹，算了，反正等会儿大放看我没过去会来找我的。”陆子麒左看右看，找了器材室里的一张床坐下，这是篮球队教练准备的用来临时休息的，他等着有点尿急，就窜进了器材室的厕所里。
厕所里一阵水声，陆子麒拉上裤链，正要走出厕所的时候，篮球队教练用钥匙打开器材室的门，带着肖放走了进来。陆子麒从门缝看去觉得他们俩有什么事情的样子，就被堵在了厕所里，借门缝看着两人。

“教练，找我什么事？你快点说，我同学还在等我。”肖放挠着后脑勺，漫不经心地乱看着，时不时抖抖黏在身上的篮球服，想让自己凉快一点，他双脚站不住，穿着最新款AJ不停地动着，手不停抠着器材室的桌子。
“没什么事，就想和你聊聊最近篮球队的一些问题，你是篮球队队长，得担起责任，别老想着玩，去，把那边桌子上的那张表拿过来。”教练严肃地斥责道。
肖放随口应了一声，走到桌子边拿起那张表，突然，他感觉后颈一阵刺痛，迅速转身向后退了几步，他摸向后颈，发现一根针深深扎进他的后颈肉里，而教练对他邪笑着，站在一旁看着他。肖放的眼前越来越模糊，走一步都要晃三下，他扶着墙往器材室的门走去，走到一半就摔在了地上。
教练朝肖放走去，从怀里拿出一块磁石放到了肖放的后颈上，用力往外一拉，深深刺进去的针被他拔了出来，针一拔出来，肖放的身体像泄掉的气球一样软了下去，教练笑着把肖放的皮拎了起来，那双AJ掉在了地上，其他的衣服还有篮球袜都还黏在肖放的皮上。

陆子麒从门缝里把器材室里发生的一切看得一览无遗，他惊诧害怕地捂住嘴，生怕出一点声被教练发现，最后变成和肖放一样的下场。只见教练拎着肖放的人皮，把肖放穿着的篮球服和篮球裤拉了下来，又把黏在肖放身上的黑丝篮球紧身衣、紧身裤以及篮球袜扯了下来，然后就随手把肖放的人皮往床上一丢，双手背向后颈摸着什么。
教练把脖子上挂着的哨子取了下来，背着对厕所门口把身上的教练服脱了个精光，包括内裤也扯了下来。

“嗯，啊，嗯……”陆子麒从门缝看去，教练此时正背对着他，双手摸向后颈的一块地方，摸到一处用力一扯，教练的背上顺着脊椎裂开了一条狭长的缝隙，里面露出了另一个人的后背，比教练的皮肤稍微白一点，只见“教练”在原地艰难地扭动着，双手抓住后颈裂开的缝往前很用力地扯着，不一会儿另一个人的头从教练的头里拉了出来，他转了过来，大口地喘着气，整张脸挂满了一种粘稠的液体。陆子麒看着他那张脸，不认识，但觉得有点眼熟。教练双目空洞的头挂在那个人的胸前，那个人邪笑着拍了拍教练的头，继续在陆子麒眼前把教练的整张皮给脱了下来，浑身都黏糊糊的。那个人拎着教练的整张人皮，走近厕所挂到了厕所前面的衣挂上，吓得陆子麒缩回了身，听着脚步声走远后，陆子麒又从门缝里看出去，那个人已经赤身裸体坐在了器材室的床上，拿过肖放的人皮埋头深深地闻了一口，一脸满足。那个人把肖放的人皮翻了个面，找到之前戳的那个针眼，用力一撕，顺着背线把肖放的皮给撕了开，他坐在床上，像穿裤子一样，迫不及待地把自己的一只脚伸进肖放的脚里，但因为教练皮和肖放皮两个都大汗淋漓的双重原因，那个人伸脚的时候十分得艰难，用力拉了好久才把一只脚给穿好，如法炮制穿完另一只脚，那个人把肖放的脚趾和自己的脚趾一一对齐，提着人皮从床上站了起来，肖放的皮渐渐往上，包裹住那个人的双腿，他把自己硬挺的宝贝往肖放疲软下去的皮里一顶，看样子比肖放的还大，突然，一股奶白色的浊流从肖放的宝贝里直直射了出去，射到了墙上。
“哟嚯，这小子。”那个人不禁说了句，但没继续多说，继续把自己的双手套进肖放的手里，套完用力地挺起肌肉，心满意足地看着肖放强健的身体，现在，只剩下肖放的头垂在了那个人的胸前，不，准确来说应该是肖放的胸前，他抱起肖放的皮，艰难地把自己的头往肖放的后脑勺一埋，埋进了肖放的头里，对齐五官，他把肖放的人皮往后用力一拉，静静等候，肖放背后裂开的缝开始慢慢愈合，只留下后脖颈上那个微微红着的针眼。“肖放”睁开了眼，抓了抓自己全是汗的头发，突然冲进了厕所里。
陆子麒被挤到门后，幸亏厕所的门后正好可以躲一个人才让他没被发现。那个“肖放”对着镜子邪魅一笑，用力拉扯开肖放的皮，里面那个人的皮露了出来，他重新把眼周的皮贴牢，揉着肖放的大胸又从厕所里冲了出去。陆子麒默默带上厕所的门，伪装成是厕所门自动带上的样子，他顺着门缝往外面看去，那个人已经拿起了肖放的内裤套了进去。
他正艰难地穿上肖放全是汗的篮球紧身服，紧致的黑丝把肖放的身材表现得超级抓人，他继续拿起宽松的篮球裤穿上，又套上写着肖放最喜欢的球星号码的篮球服，然后重新坐回床上套起肖放的篮球袜，把脚蹬进那双AJ里，帮肖放的AJ系了个新的花式鞋带，从床上坐起来兴奋地跳了几下，把地上教练的衣服卷成一团嫌弃地丢到了床上，拿过桌子上放着的一个圆盒子和器材室的钥匙，跑出了器材室。

等到陆子麒确定那个“肖放”真的走远后，才偷偷摸摸从器材室的厕所里探出头来，他走进空无一人的器材室，旁边就挂着教练的人皮，他小心翼翼地摸了一下，又摸了自己的脸，确实是人皮的感觉，他把教练的人皮取了下来翻了个面，果然教练背后的缝已经愈合了，后颈的地方有一个微红的针眼。他突然想起还和“肖放”约好了饭，陆子麒赶紧把教练的人皮挂回原位，跑出了器材室。
气喘吁吁赶回寝室，寝室里的另外两个室友已经回家了，陆子麒迎面撞上了抱着个脸盆正要洗澡的肖放。
“我有事……”陆子麒傻傻地，不知道该说什么。
“算了算了，食堂总请得起吧，请我吃顿饭，这件事就当没发生过，让我堂堂校草，篮球队第一帅哥等你陆子麒，便宜你了。”肖放拍拍陆子麒的肩膀，抱着脸盆走进浴室，陆子麒觉得这一切很不真实，肖放还是以前的肖放，说话神态都一模一样，直到他看见肖放后脖颈上那个微红的针眼，才把他又拉回了现实，眼前的这个肖放，是个刚刚把真正的肖放和篮球队教练变成人皮穿在身上的危险对象。

“肖放”很快冲了个澡出来，还是只穿着一条内裤，他打开自己的衣柜随便拿了条运动裤和一件宽松连帽卫衣套了进去，穿上船袜把脚伸进了一双基础款Air Force 1里，起身跳了两下拉过浑浑噩噩的陆子麒冲向了食堂，陆子麒是懵的状态被眼前的这个肖放削了一顿。

“别犯懵了，半夜丁少爷不是攒了个局吗，你还不睡？算了，你不睡我先睡为敬，今天打球打得累死了。”肖放神经大条地把鞋子蹬掉，把那个圆盒子和钥匙放到一边，扑进被窝里很快睡了过去，响起了很重的鼾声。

陆子麒坐在自己的床上，小心翼翼地确定“肖放”真的睡死过去后，拿了几粒自己吃的安眠药泡水，硬是倒进了“肖放”的嘴里，这下“肖放”是彻底醒不过来了。陆子麒掀开肖放的被子，把他身上的衣服给拔了个干净，他找到后颈上的那个微红的针眼，轻轻地撕开，肖放的皮从他手里一滑弹回了那个人的后颈，愈合后又只剩下了那根针眼，这也就难怪之前那个人脱教练的人皮的时候那么用力了。他重新戳进那个针眼用力一扯，把肖放的背拉出一条狭长的缝，把整张皮从那个人的身上揭了下来，肖放的人皮空荡荡的躺在地上，后背很快又愈合成一个小洞，只是还是瘪的。陆子麒不知道下一步该怎么办，他拿过那个圆盒子打开，里面装着把肖放变成皮的那根针和那块磁石，他决定试试，拿起磁石放到肖放人皮背后的那个针眼上，那个针眼居然愈合了，肖放也开始慢慢鼓胀起来，他正高兴着，突然后脑勺被人重重一击，眼前一黑失去了意识。

等到陆子麒再醒过来，已经不在寝室里了，他的双手双脚被绑在器材室的床的四角，“肖放”正坐在床边拿着一根棍子戳着陆子麒的宝贝，“你小子厉害啊，我就觉得你有什么事瞒着我，要不是我留了一手假装吞药……对了，我有个东西给你看看，挺有意思的。” 

“肖放”拿出手机，给陆子麒放了一段视频，视频里陆子麒和肖放都昏倒在地上。那个人把棍子随手一扔，把自己含着的安眠药重新吐回杯子里，灌进了已经恢复身体只是还没有意识的肖放的嘴里，硬是让他全喝了下去，然后那个人走到陆子麒的旁边，拿出圆盒子里的针对着陆子麒的后颈深深一扎，然后用磁石把针吸了出来，陆子麒整个人也瘪了下去，他拎起陆子麒的人皮，抖掉陆子麒身上的衣服，坐在肖放的床上把陆子麒的皮给穿了起来，很快，一个赤身裸体的陆子麒从肖放的床上站了起来。他坏笑着扛起昏迷的肖放，对着肖放的雏菊一个硬挺就探了进去，肖放的里面很紧，痛得“陆子麒”很兴奋，整张脸红通通的，他在肖放的身体里留了好几道痕迹，又让肖放在陆子麒的身体里留下了足迹，才心满意足地把肖放又变成了一张人皮，连身上的陆子麒都没脱，直接就穿了起来。陆子麒看着视频里的“自己”把肖放穿了起来，心底产生了一种奇怪的感觉，陆子麒的皮被那个人这么一搞，浑身出汗黏糊糊的，那个人先去冲了个凉，再回来轻松地穿上肖放的人皮，套上肖放的卫衣和裤子，蹬上Air Force 1白板鞋，把陆子麒的衣服鞋子装进一个包里，背着离开了寝室。视频到此就结束了。

“要是我把这段视频剪辑一下传到校网上，你们俩在学校也就不用待了，说吧，怎么发现的？”那个人逼问道。

“就是你和肖放进器材室的时候，我没走。” 

“肖放”一脸惊诧，那时候器材室里有一个大活人他居然没发现，不过他很快就镇定下来，“我看你说的也不是假话，这样吧，你不必担心，我的目标不是你，也不是肖放，等到我得到我的目标后，肖放会还给你的。”那个人把陆子麒的束缚解了开，把包扔给了他，“快点换上，丁少爷的深夜轰趴要开始了，我在门口等你。”

陆子麒没办法，把柄被人抓着，只好乖乖地把衣服换好，和“肖放”打的打到了市区的一间KTV，丁少爷包下了这里的总统包厢，请了几个好友一起唱个通宵。陆子麒刚走进包厢坐下，“肖放”就不见了，可能是去买东西了吧。

丁泽看到陆子麒一副心神不宁的样子，凑到他身边坐了下来，“怎么了子麒，周末了你还一副苦瓜脸？”

“没什么没什么丁少爷，可能是作业太多了吧，你唱你的，不用管我，对了，我问你一句，你最近有得罪什么人吗？”陆子麒一边观察着门口一边偷偷问道。

“得罪什么人？没有吧，我最近很守纪律的，哦，我想起来了，之前有个不长眼的撞了我，还和我打了一架，现在被我搞退学了，我特地让校长把他的照片贴在了公告栏上。”丁泽这样一说，陆子麒才想起那张脸在哪里见过了。

“先不和你说了，我好像水喝得有点多有点尿急，我先去上厕所。”丁泽慌慌张张就跑出了包厢。

丁泽上厕所的时间似乎太长了，等了好久包厢的门才缓缓打开，走进来的却是肖放，他一边揉着后颈一边走了进来，看见陆子麒后朝他走了过来。陆子麒看了眼，肖放背后那个微红的针眼已经消失了，肖放一直向他吐槽着自己的记忆出现了断片，陆子麒尴尬地笑了笑，关注到后一脚走进包厢的丁泽，他找了陆子麒的另外一边坐了下来。

“喂，丁少爷，过来唱歌啊！”有人在喊他。

“好，我马上来。”丁泽转了个头，正好让陆子麒看到了丁泽后颈上的一个针眼，丁泽转回头朝陆子麒不明含义地笑了笑，拍了下他的肩膀，走向了点歌台。

大约一个月后，假期结束，返校日的前一天傍晚，陆子麒和肖放比另外两个室友先回来一天。肖放过了一个寒假，感觉把整个衣柜换了一样，所有的衣服全部变了个样，他看陆子麒天天一身黑白灰的，实在受不了了。
“陆子麒，过来！”

“怎么了？” 

“你说你这天天不是黑白就是灰的，和你出去吃饭都丢脸，脱了脱了，我来给你搭。”陆子麒被肖放拉扯着把身上的衣服扒了个干净，肖放翻着自己的衣柜，从里面拿出了一件黄色卫衣和牛仔外套，一条工装裤和一双黄色converse，“就这些了，给我穿上！” 

“不好吧，我觉得黑白灰挺好的啊……”

“你再多说一句，我打爆你的狗头。”

“诶诶诶，好，我马上换，还请肖哥高抬贵手。”陆子麒迅速地把工装裤套上，穿上皮带，把头套进宽松加绒的黄色卫衣里，他已经穿好了一只鞋，正给另外一只系着鞋带。

“你换着，我先去个厕所。”肖放走到厕所门口，厕所就在寝室的门口，还没等他进去，寝室的门就被猛地敲响了，他生气地打开寝室门，发现外面站着的居然是篮球队教练，“教练，你怎么来了？” 

教练面色阴沉，看着似乎心情很不好，他不说话，突然冲进寝室，一手拿着针刺向肖放的后颈，针深深没入肖放的后颈肉里，肖放倒在地上挣扎了一会儿就昏了过去。随后，他拿出磁石，正想把针拔出来的时候，陆子麒突然冲了过来，抓住肖放的一只手往另一个方向拉，教练赶紧抓住肖放的另一只手臂阻拦着，两人就这么僵持着。

“你跑来干嘛！”陆子麒厉声质问道。

“放手。”教练声音很低沉，却很有压迫力。

“你先说你要干什么！”陆子麒死死抓着肖放的手，和教练一直僵持着，教练狠狠瞪着陆子麒，陆子麒同样回以恶狠的眼神。

“放手！”

“不放！” 

“丁泽丢了。”篮球队教练突然把手松了开，昏迷过去的肖放立马顺着陆子麒的方向倒了下去，他趁陆子麒措手不及，立马上前用磁石把针往外一拉，把针收了回去。

陆子麒一个猝不及防，看着怀里昏过去的肖放慢慢地泄气下去，变成了一张扁平的人皮，教练趁陆子麒不备，一个抽拉把肖放的人皮抽了去，“好了，别宝贝着了，我没时间跟你在这耗，就借你的兄弟肖放同学一下，陪我去一趟丁泽家，有个狗把丁泽的皮抢走了。” 

“什么？”陆子麒觉得十分不可思议。

“你坐着，我边穿边说，就两天前我去健身房……”

两天前，市中心健身房。丁泽穿着一身笔挺的黑西装走进这间位于市中心原本就属于自家产业的健身房，健身房生意异常火爆，不仅那些特意挑选的健身教练，还有很多客人都是丁泽的菜，丁泽的下面早就按捺不住了，胀胀地鼓起一大包，却只能端着，慢慢走到那间专属于他的办公室。走进办公室里，丁泽立马激动地把门关上，打开办公室一旁放着的置物柜，里面摆着丁泽常穿的几套健身服，他拿了一套出来，放到了办公桌上。
丁泽握住皮鞋的后跟，稍稍用劲往下一拉，他感觉自己被紧紧包裹住的脚突然一股凉意，还没来得及脱下黑丝袜，办公室的门就被某人推开了。

丁泽看了眼来人。

“有事吗，川哥？”

“没有老板，我看你刚刚好像不太舒服，所以特地给你泡了杯咖啡。”健身房的大经理陆子川，陆子麒的孪生哥哥，端着个托盘走到了丁泽旁边，“咖啡是刚刚泡好的，这里是奶精和砂糖，老板慢用。”

“嗯好，放着吧，还有事吗？”丁泽看了眼咖啡，嘴角扬起一丝不明含义的坏笑。

“没有了。”

“好，那你先出去吧。”丁泽看着陆子川走出办公室，端起咖啡闻了一下，笑着把杯子里的咖啡倒进了一边的垃圾桶，“啧啧啧，这药味道也太大了吧，果然是陆子麒的哥哥，脑袋都是一样笨的。”

丁泽继续忙着换装，捏住丝袜的边和头把整条丝袜给扯了下来，他赤脚站在办公室的地毯上，抖下比较宽松的外套，打开皮带扭开裤裆的纽扣，刚拉下裤链，西裤就顺着丁泽的大腿滑到了地上。脱掉雪白的衬衫，丁泽从那包健身服里抽出紧身的黑色棉背心套了进去，然后坐在办公室的软椅上穿上了黑丝紧身的健身裤和黑色棉袜，捡起掉在一旁的宽松的短裤穿了起来，戴上运动手环和把头发束上去的发带后，走向另一边的鞋柜，从鞋柜里拿出一双Nike Metcon 4黑白训练鞋，把脚探进去敲了敲脚，走出了办公室。

“哈哈哈，川哥，又失败了吧？”

“你谁啊，要你多嘴，还不滚去上班？”陆子川正在气头上，一把抓过说话人的工牌，随意地看了一眼，上面写着陈双林这个名字，“行，陈双林是吧？我记住你了。”

看着陆子川生气地离开后，陈双林把怒气全部加注到了正带着耳机在跑步机上肆意挥洒汗水的丁泽身上，他愿意来这家健身房当教练，完全是为了陆子川，当时经过这间健身房的时候，远远地看见了陆子川监督学员工作的模样，他的心头顿时生出一种爱慕之情，可事与愿违，他来这没几天，就知道陆子川的心思完完全全在丁泽身上！丁泽，丁泽，要是他是丁泽就好了……

陈双林漫不经心的，弄得几个新来的学员不满意地到前台把卡退了，他一个下午一直不时地关注着丁泽的动静，大概是下午四点的时候，他瞥见丁泽看着想似乎想离开的样子，立马停下手里的活，躲进了丁泽的办公室，他两手握着一根半个拳头粗的棍子，藏在办公室的门后。

丁泽一边用脖子上的毛巾擦着汗，一边往办公室走去，之前无论是穿体育老师的还是肖放的皮，生活都没有穿丁泽大少爷的这么有意思，每天都能有个新花样。他最后抹着脸走进办公室，顺带着把门带上时，根本没看见陈双林，他只感觉到后脑勺一阵剧痛，眼前一黑，倒在了地上。

陈双林丢下棍子，立马把办公室的门反锁起来，把昏过去的丁泽拖到了办公桌的后面，他把丁泽身上的衣服全部扒了下来，正想拍丁泽的艳照。突然，丁泽后颈上的一个微红针眼引起了他的注意，他试探着往那个针眼戳去，食指把那个针眼撑开一个大口子，他吓得赶紧把手抽回来，以为那口子会崩出血来，可那口子却自己收缩了回去，重新变成一个微红的针眼，丁泽也没什么事的样子。

陈双林脱得干干净净，坐在办公室的老板椅上，双腿夹住丁泽的头，笔挺的尤物直直顶上丁泽的额间，他腾出手小心翼翼地撑开那个微红的针眼，顺着背线稍稍拉开一点，把丁泽的头皮拉了下来，里面冒出另一个人的头，吓了他一跳！ 

陈双林内心蠢蠢欲动，这样不正合他意吗？ 

陈双林双腿松开那个人的头，让那个人平平趴在地上，小心翼翼拉开丁泽的皮，把丁泽整张皮揭了下来，陈双林拎着丁泽的皮，无法置信，但又恨不得马上把自己塞到丁泽的人皮里面。陈双林坐在老板椅上，双腿双脚穿进丁泽的皮里，丁泽瘫软的腿立马被陈双林健硕的双腿撑了起来，他激动兴奋地对齐每一个脚趾，跳到地上把丁泽的皮往上微微一拎，笔挺的大龙猛地把丁泽疲软的皮撑了起来，继续伸进双手，把丁泽的头往自己的头上一套，拉拉眼孔对对嘴唇，很快丁泽的脸已经完美地贴合到他脸上，后背的缝隙愈合上后脖颈留下一个微红的针眼，陈双林顺着丁泽的脸，从左耳朵尖滑过下巴，摸过丁泽性感的胡茬滑到右耳朵尖，露出一丝邪魅狂狷的笑容。他不屑地看了眼地上那个昏倒的人，坐在老板椅上叉开自己，也是丁泽的双腿，掐住撑开那个人的嘴，把丁泽的尤物塞到了那个人嘴里，一管浑浊的暖流瞬间喷进那个人的嘴里，直直顺着滑了下去，有些漏了出来。

陈双林嫌弃地抓着那个人的脸，厌弃地把那个人的头摔到了地上，他赤身裸体着走到置物柜里，拿了一条新的干净的内裤套上，然后穿进衬衣夹后翻箱倒柜拿出了一件崭新的白衬衫套上，对着穿衣镜里坏笑着的丁泽的脸慢慢扭上纽扣，接着他穿上袜夹套上纯黑条纹丝袜，又拿出一条修身的条纹西装对着镜子穿了起来，他懒得扎领带，把脚塞进皮鞋里后就从办公桌的抽屉里拿出手表、钱包配好，他小小跳了一跳顺顺衣服，提过地上那个人把他塞到了置物柜里反锁掉，抓起办公桌上放着的丁泽的手机，指纹解锁打开了丁泽的手机，他邪邪一笑，左右调整了一下领口，走到健身房楼下看着发光的屏幕，他打开署名为陆子川的对话框，忍不住笑着简单打了一行字：“某街某牌号，某日晚六点，不见不散。”

“事情大概就是这样了。”教练边说着，已经三下五除二穿上了肖放的人皮，让陆子麒深刻怀疑，之前他不在的肖放训练的时候，这个人肯定又把玩了肖放好几次！ 
“好了别用那种眼神看我了，知道你心里想什么，要不我写个证明，保证以后再也不穿肖放，行了吗？”他被陆子麒盯得浑身不舒服，只好邪笑应和着打趣了一下，他一丝不挂地绕过眼神还是不肯放过他的陆子麒，捡起掉在地上的内裤穿上后团起肖放剩下的衣服，随手一抛扔到了衣柜里。然后，他从衣柜里拿出一件白T和湖蓝色卫衣套上，选了一条阿迪的搭配神裤和一双纯黑长棉袜换好，最后从肖放的鞋柜里挑了一双AJ1 Mid黑蓝换好，扯上陆子麒赶往丁泽家。

丁泽家住在市郊的别墅区，除非是私人接送，要不然就要落得和陆子麒和肖放一样转车好几趟的下场，正当他们俩终于到了丁泽家门前，可门口长得挺标致的小保安却说他们俩没有预约，不肯让他们进去，还说丁泽今天有特别重要的客人要见。他们两个站在墙边一筹莫展，突然发现另一个方向走来一个熟悉的身影。

“诶，我哥？”陆子麒一眼瞥见了陆子川。

“那不是正好？川哥！川哥我们在这里！”肖放兴奋地跳着朝陆子川挥手，把陆子川招了过来。

“你把我哥叫过来干嘛？”陆子麒疑惑地看着肖放。

“别吵，你等会儿就知道了。” 

“小麒，放子，你们怎么会在这？”陆子川满脸暖笑着朝他们走了过来，一身简单的运动服和一双舒服的运动鞋。

“我们来找丁泽出去，谁知道这小子说今天有什么大事，不来了，我们俩就堵到他家来，谁知道，嚯，说我们没有预约不见我们，一看就有事，要不川哥带我们进去？”肖放笑着问道，背后已经掏出了那根针。

“……”陆子川尴尬地挠了挠头，心想着不能让这俩小子坏了自己的好事，正张嘴想出了个借口还没说，肖放的手臂在他眼前一晃，他就感觉自己的后颈好像开了个洞，所有的力气都顺着那个洞漏了出去，他连忙抬手想要捂住，可是早已来不及了，他浑身软了下去，变成了一张人皮。

“你干嘛？”陆子麒傻了。

“没看到你哥刚才犹豫的表情了吗？他和现在那个穿着丁泽皮的狗绝对有事，或者，他和丁泽有事？”肖放抱起那一团东西拉过陆子麒到了个隐蔽的地方，把陆子川的皮从衣服里抽了出来，“好了，快点穿上！” 

“我才不穿。” 

“好啊，不穿的话，那我也就永远穿着肖放不脱了，还有你哥，也就一直当件人皮好了。” 

“卑鄙小人，我穿，我穿就是了！” 

陆子麒接过陆子川的皮，有点重，可能是陆子川在健身房工作的缘故，他不紧不慢地把身上的衣服全部脱光，装进了陆子川带来的运动背包里，满脸惊疑心里却有些好奇地把一条腿伸进了哥哥的大腿里，有些酥酥麻麻的感觉，然后，他如法炮制地把另一条腿伸进陆子川的皮里，没一会儿，人皮就紧致地贴上了陆子麒的腿。陆子麒没想到人皮这么快就愈合，所以自己笔挺着的硬物被紧绷着的皮顶着，顶得有些难受。“肖放”不经意间瞥了一眼，实在忍不住，笑出了声来，陆子麒赶紧把自己的宝贝塞进了哥哥瘫软的皮套里，最后穿进手臂卖力向后一抻，把头埋进陆子川耷拉着的后脑勺里，随即一阵冰凉的感觉，陆子川人皮背后那条大缝迅速地愈合，合上后脖颈变成了一个微红的小洞。

“好了，接下来该怎么办？”陆子麒一边拿着陆子川的衣服换上一边问道。

“你去把那个小保安引到这边来。”

小保安很顺利地被引出来，被肖放变成了一张薄薄的人皮，陆子麒自觉把着风，草丛后面一阵翕动，很快，拎着一双白手套淡然自若地套上，悠闲走出来的小保安走到了陆子麒旁边，他猜的没错，今天丁泽要见的客人果然就是陆子川。

“肖放呢？”陆子麒还是满嘴肖放肖放的。

“这里这里！”他被陆子麒气得不行，直接一把拉长小保安的眼皮，从里面隐隐约约露出来肖放的脸，百般贴到陆子麒脸旁边让他检查过后，才生气地把眼皮调回了原处，“好了好了别废话了，我不会把你心爱的肖放同学怎么样的，赶紧把那个狗身上丁泽的皮扒下来才是我今天带你来的目的，好了快点快点！” 

一路靠着小保安的记忆到了丁泽的房间前，丁泽目前在本市一个人住，父母都在国外出差，小保安轻轻叩响丁泽的房门，“少爷，人我已经领来了。” 

房间里一阵骚动后，丁泽坏笑着打开了房门，他只是简单披了件白衬衫，前面的纽扣也没扣上，修身的西装裤也像是刚刚才穿上的，裤链开着，赤着脚。

“川哥来了？请进请进。”陆子麒明显有些不知所措，僵在原地一动不动，还是小保安在背后推了他一下，他才跌跌撞撞摔进了丁泽的房间。

“小齐，去浴室帮我放一下洗澡水，温度你知道的。”小保安带上门，点过头后默不作声地进了浴室。

“好了川哥，我们之间就不用这么遮遮掩掩的吧？你对我有什么小心思，我都知道，我也不是不开放的人，川哥你也跟了我这么久，就算是对你的补偿好了。”陆子麒被一路推搡着推到了丁泽的大床边，硬是让丁泽按倒在绵软的大床上，丁泽脱掉西装裤和白衬衫，一个跃起跳上床骑在了陆子麒身上，他扯开陆子川的运动服，从里面爆开一件紧贴着胸肌的白背心。

“川哥，身材不错啊，没少费心思吧。”丁泽伏下身，拉开陆子川紧贴着的白背心，从下面紧贴着陆子川的肌肉慢慢爬了上去，紧身的白背心硬是让丁泽撑成了两人份。陆子麒看着那颗硕大的头颅顶着白色的面料不断前进，一路穿过背心的口子爬到了他面前。丁泽深情款款地看着他，双手也穿过背心，现在两人被一件背心套住，紧得可以，他对着陆子川的嘴，深深吻了下去，正想用灵巧的舌头撬开陆子川紧闭着的牙关，突然，陆子川突然像一只考拉，紧紧地把他锁死在床上，让他有些猝不及防。

“喂，快点，已经抓住了！”陆子川突然喊道。

丁泽起初还有些疑惑，他艰难地转过头，看到了阴笑着从浴室里走出来的小齐，小齐阴险地看了丁泽一眼，背过身朝他指了指背后的那个红点，穿着丁泽人皮的陈双林瞬间就明白了，他中计了。他奋力地在陆子川的怀里挣扎着，可紧身的白背心和陆子川很快就让他没了力气，只能眼睁睁地看着小齐朝他走来。那双戴着白手套的手瞄准着他后脖颈的那个微红的针眼，用力地把丁泽的皮扯开了一条缝，把他的头从丁泽的头里捞了出来，突然一阵轻微的刺痛，陈双林感觉自己的身体被开了一个洞，所有的力气都被抽了出去，身体软了下去，变成了一张皮。

丁泽家的事情结束后，肖放如约被复原了，但丁泽还是被穿在那个人身上，不过听说倒霉的教练和保安小齐也被复原了，至于陈双林，还在丁泽家的衣柜里挂着。

很快，到了正式上课的日子。
“喂，丁少爷你听说了吗？”一同学对他耳语道。

“什么？”丁泽一副淡定的表情。

“李妈怀孕回去生孩子了，所以我们班换了个新来的语文老师，好像姓陈，听说以前是他们师范学校的校草……”正说着，一个穿着精致面容姣好的男教师走上了讲台，铃声正好清脆打响，那个男教师穿着一身休闲的鸦青色条纹西装，戴着一副多边形近圆框眼镜，黑棕色的头发简单收拾过，右手一只雅致的手表，脚踩一双奢侈品牌的小白鞋，他俯身从粉笔盒里拿出一支白粉笔，在黑板上写下了三个字，然后转过身眯着眼朝讲台下惊诧的同学们淡淡一笑。

“同学们好，我叫陈一木，是你们新来的语文老师，大家可以叫我陈老师或者一木老师，以后熟了可以叫一木哥，那么自我介绍就这样，大家拿出语文书翻到第十页。”陈一木扶了扶眼镜，一笑就让全班女生犯起了花痴。

“好的，一木哥。”丁泽舔了舔嘴，坏笑道。

这节语文课是周五的最后一节课，丁泽一节课一点都没听进去，全程盯着陈一木那张像是从漫画里走出来的脸，无论是外表或是着装，陈一木这个人都完完全全撞进了他的爱河，他恨不得直接在课上就把陈一木变成一张人皮，下面早就鼓起一大包，躁动难耐地在位置上不停地扭着，终于，等到了一直候着的下课铃。

“今天这节课就上到这里，同学们周末要回家的注意安全，你们应该是还有一节自习课的，那过一会儿语文科代表来语文组办公室拿一下作业，好，就这样，丁泽同学，你跟我来一下办公室。”陈一木殊不知羊入虎穴，天真地带着虎视眈眈的丁泽来到了空无一人的语文组办公室里。

“丁泽同学，你上课的小动作老师都看到了。” 

“我实在忍不住了一木哥。” 

“忍不住就不要硬撑，和我打过报告就可以去厕所，老师不是那么不讲理的人，下次注意知道了吗？”

“不是这个忍不住一木哥。” 

“那是什么？” 

“一木哥，我不好说，诶？一木哥你看你后面。” 

陈一木被丁泽弄得丈二和尚摸不着头脑，他转过头看了眼背后，什么东西也没有，正想转回来好好教训一下这个屡教不改的富家小子，谁曾想突然后脖颈一痛，有些冰凉的感觉，摸了摸发现一根针深深埋进了他的后脖颈。他从椅子上蹭的一下站起来，可双腿突然一软，眼前一阵发昏，摔在了阴险笑着的丁泽面前。丁泽一把接住昏死的陈一木，把他扶到了办公室的靠椅上，拿下他的眼镜，摘下他的手表，内心狂喜地解开陈一木西装外套的纽扣，帮他脱下来放到了一边，然后丁泽的手就开始不听使唤，里面白衬衫的纽扣才解开两颗，便抓住陈一木的衣领，张嘴轻轻在他的脖侧咬了一口，埋了好一会儿才收回心，帮他脱掉了白衬衫。白衬衫里的内搭被不耐烦地扯掉，丁泽开始对陈一木的下半身下手了，脱掉陈一木的白板鞋和船袜，帮他解开纽扣和裤链，从里面露出了一顶高耸入云的帐篷来，稍稍把内裤往下一拉，一根暴起青筋坚硬笔挺的尤物弹了出来，笔直地对着丁泽的嘴巴。这一下瞬间让丁泽的理智全线崩溃，他粗蛮地架起陈一木的双腿，用力一扯把陈一木的西裤连带着他的内裤一起扯了下来，丁泽看着已经一丝不挂昏迷着的陈一木，躁动难耐开始宽衣解带，三下五除二就把衣服脱了个干净。
