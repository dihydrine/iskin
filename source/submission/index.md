---
title: 文章投稿
date: 2022-01-31 01:18:46
article:
    licenses: false
comment: false
donates: false
---

<style>
  .main.page.about {
    top: 50%;
  }
  .main.about .content {
    margin-top: 2rem;
    text-align: center;
    font-style: italic;
  }

  .main.about .content h2 {
    font-style: normal;
    font-family: "Microsoft Yahei", "PingFang SC", serif;
    margin-bottom: 1.25rem;
  }

  .main.about p.contact {
    font-style: normal;
    color: grey;
  }
</style>
<div
  class="main page about"
  style="
    position: relative;
    margin: 0 auto;
    min-height: 100%;
    padding: 7rem 0 9rem 0;
  "
>
  <div class="header site" style="text-align: center">
    <div
      class="logo"
      style="
        margin: 0 auto;
        width: 10pc;
        background-size: cover;
        background-repeat: no-repeat;
        background-image: url(/apple-touch-icon.png);
        height: 160px;
      "
    ></div>
  </div>
  <div class="content">
    <h2>你好哦！</h2>
    <p><b>你可以通过下方投稿文章到 illusory skin</b></p>
    <p><div class="heyform" data-id="i1UshkER" data-mode="1" data-button-text="点击投稿"></div><script src="https://my.heyform.net/embed?v=2022.5.1"></script></p>
    <p>「或发送邮件到 i@101069.xyz」</p>
    <p class="contact">
      ……？<s>床上交流</s> 群内交流
    </p>
    <p class="contact">
      &nbsp;&nbsp;&nbsp;
      <a href="mailto:i@101069.xyz" target="_blank"><i class="fas fa-envelope"></i> Mail</a>
    </p>
  </div>
</div>