# illusory skin

一个小众XP文章储存点。

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://bitbucket.org/dihydrine/iskin/src/main/LICENSE)

本篇 README.md 面向希望部署本站的开发者（兴趣爱好者）
 
## 目录

- [一键部署](#一键部署)
- [文件目录说明](#文件目录说明)
- [使用到的项目/服务](#使用到的项目/服务)
- [版本控制](#版本控制)
- [作者](#作者)
- [版权声明](#版权声明)

### 一键部署

1.申请 [Vercel](https://vercel.com/signup) 账号。

2.点击以下按钮将本仓库一键部署到 Vercel。

[![](https://vercel.com/button)](https://vercel.com/import/project?template=https://bitbucket.org/dihydrine/iskin/src/main/)

3.连接 Github、Gitlab或者是 Bitbucket 仓库以储存这个项目。

4.等待自动部署即可正常访问。

### 文件目录说明

本项目基于静态网站生成器 [Hexo](https://hexo.io/) ，详细内容不再赘述。

eg:

```
filetree 
├── _config.yml
├── package.json
├── scaffolds
├── source
|   ├── _drafts
|   └── _posts
└── themes
```

### 使用到的项目/服务

- [Hexo](https://hexo.io/) 生成网站。
- [Icarus](https://github.com/ppoffice/hexo-theme-icarus) 作为 Hexo 主题。
- [Valine](https://valine.js.org/) 评论系统。
- [Backblaaze](https://www.backblaze.com/) 对象存储。
- [Cloudflare](https://www.cloudflare.com/) CDN分发。
- [Vercel](https://vercel.com/) 站点部署。

### 版本控制

该项目使用Git进行版本管理。您可以在 [Commits](https://bitbucket.org/dihydrine/iskin/commits/) 查看过去版本。

### 作者

Dihydrine@gmail.com

### 版权声明

该项目签署了MIT 授权许可，详情请参阅 [LICENSE](https://bitbucket.org/dihydrine/iskin/src/main/LICENSE)